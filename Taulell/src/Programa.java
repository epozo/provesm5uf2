import java.util.Scanner;

public class Programa {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Taulell t = new Taulell();
        Finestra f = new Finestra(t);
        
        Scanner sc = new Scanner(System.in);
        
        sc.nextLine();
        
        //cas 1: Hundir la flota
        
        System.out.println("cas 1: Colors - Enfonsar la flota");
        
        t.setActcolors(true);
        t.setActimatges(false);
        t.setActlletres(false);
        int[] colors={0x0000FF,0x00FF00,0xFFFF00,0xFF0000,0xFF00FF,0x00FFFF,0x000000,0xFFFFFF,0xFF8000,0x7F00FF};
        t.setColors(colors);
        f.setActetiquetes(true); //les etiquetes i titol com que no tenen a veure amb la matriu es tracten per la finestra!
        String[] etiquetes={"Dispars:10","Portavions:1","Cuirassats:0","Creuers:0","Submarins:1"};
        f.setEtiquetes(etiquetes);  
        f.setTitle("Enfonsar la flota");
        int[][] matriu =
        	{
        		{0,0,0,0,0,0,0,0,0,0},
        		{0,0,5,3,3,0,0,5,0,0},
        		{0,0,0,0,0,0,0,0,0,0},
        		{0,0,5,0,6,0,0,0,0,0},
        		{0,0,0,0,6,0,5,0,0,0},
        		{0,0,0,0,6,0,0,0,6,0},
        		{0,0,5,0,6,0,0,0,0,0},
        		{0,0,0,0,0,0,0,0,0,0},
        		{0,0,0,0,0,0,0,0,0,0},
        		{0,0,0,0,2,4,1,7,8,9},
        	};
        

        		
        t.dibuixa(matriu);
        sc.nextLine();
        
 System.out.println("cas 6: Comprovacio mouse i teclat");
        
        System.out.println("prem en una casella amb el ratol� i apreta una tecla del teclat. Despr�s prem intro");
        
        sc.nextLine();
        System.out.println("l'ultima casella clickada es:  fila "+t.getMousefil()+"   columna: "+t.getMousecol());
        System.out.println("l'ultima tecla premuda es:   "+f.getActualChar());
        System.out.println("ara hauria d'estar en zero. Aixo reflecteix les tecles"
        		+ "que vols apretar i un cop actuades no vols que quedin en memoria:  "+f.getActualChar());
        System.out.println("i aquesta es l'�ltima tecla premuda sense quedarse en blanc: Aix� �s per quan"
        		+ "vols un sistema amb memoria: "+f.getUltimChar());
        
        
        
      
	}

}
