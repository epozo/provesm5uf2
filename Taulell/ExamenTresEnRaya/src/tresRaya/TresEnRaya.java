package tresRaya;

import java.util.Scanner;

public class TresEnRaya {
	final static int MAX = 3;
	final static char casillaTapada = '*';
	static Scanner reader = new Scanner(System.in);
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		char resp;
		
		do {
			jugar();
			resp = obtenerResp();
		} while (resp == 's' || resp == 'S');
	}
	
	private static void jugar() {
		// TODO Auto-generated method stub
		int jugadorActual;       ///Número del jugador que tira en cada moment: 1 -j1 / 2 - j2
		int numTiradas;          ///Comença amb valor 0 i arriba a 9 quan tot el tauler estigui ple: Fi de la partida
		char [][] tauler;       ///Tauler de tirades
		boolean guanyador;          ///Comença amb false i es ficarà a true quan algun jugador hagi guanyat
		char resp;
		
		///Codi d'una partida
		tauler = new char[MAX][MAX];
		
		inicializar(tauler, casillaTapada);     ///Prepara el tauler per començar a jugar. Totes les caselles a '*'
		jugadorActual = 0;			  //Jugador 1 - símbol X; Jugador 2 - símbol O
		numTiradas = 0;

		do
		{
		    mostrar(tauler); 
		    jugadorActual = ObtenirJugador(jugadorActual);
		    tiradaJugador(tauler, jugadorActual);
		    numTiradas ++;
		    guanyador=comprovar(tauler);
		    resp = obtenerResp();
		} while(numTiradas < MAX*MAX && guanyador == false && ((resp == 's' || resp == 'S')));

		mostrar(tauler);
		finalitzarPartida(numTiradas, guanyador, jugadorActual);
	}

	private static void finalitzarPartida(int numTiradas, boolean guanyador, int jugadorActual) {
		// TODO Auto-generated method stub
		
		if (guanyador == true){
	        if (jugadorActual == 1)
	        	 System.out.println("Ha guanyat el jugador 1");
	        else
	        	 System.out.println("Ha guanyat el jugador 2");

	    }
		else {
			if (numTiradas == MAX*MAX)
				System.out.println("Joc acabat amb TAULES");
			else 
				System.out.println("Partida no acabada");
		}
			
	}

	private static boolean comprovar(char[][] taula) {
		// TODO Auto-generated method stub
		int i,j;
		boolean guanya;
		char car;

		///comprovem si es guanya per files
		guanya = false;
		i = 0;

		while (i < MAX && guanya == false)
		{
		    car = taula[i][0];
		    j = 1;
		    while (j < MAX && taula[i][j] == car && taula[i][j] != '*') j++;

		    if (j == MAX) ///a la fila j totes les caselles tenen el mateix valor
		        guanya = true;

		    i++;
		}

		///Si per files no hi ha guanyador, comprovem si es guanya per columnes

		j = 0;

		while (j < MAX && guanya == false)
		{
		    car = taula[0][j];
		    i = 1;
		    while (i < MAX && taula[i][j] == car && taula[i][j] != '*') i++;

		    if (i == MAX) ///a la columna i totes les caselles tenen el mateix valor
		        guanya = true;

		    j++;
		}

		///Si per files ni per columnes no hi ha guanyador, comprovem si es guanya per diagonals

		if (guanya == false)
		{

		    ///diagonal principal
		    i = 0;
		    car = taula[0][0];

		    while (i < MAX && taula[i][i] == car && taula[i][i] != '*') i++;

		    if (i == MAX) ///a la diagonal principal totes les caselles tenen el mateix valor
		        guanya = true;
		}

		if (guanya == false)
		{
		    ///diagonal secundaria
		    i = 0;
		    car = taula[i][MAX-1-i];

		    while (i < MAX && taula[i][MAX-1-i] == car && taula[i][MAX-1-i] != '*') i++;

		    if (i == MAX) ///a la diagonal secundària totes les caselles tenen el mateix valor
		        guanya = true;
		}

		return guanya;

	}

	private static int ObtenirJugador(int jugadorActual) {
		// TODO Auto-generated method stub
		jugadorActual = jugadorActual % 2 + 1;
		System.out.println("Li toca al jugador " + jugadorActual);
		return jugadorActual;
	}

	private static void tiradaJugador(char[][] tauler, int jugadorActual) {
		// TODO Auto-generated method stub
		int fila, col;
		boolean correcte = false;
		
		do {
			System.out.println("Casella a seleccionar: \nFila: ");
			fila = ontenirNum();
			System.out.println("Columna:  ");
			col = ontenirNum();
			if (tauler[fila][col] == casillaTapada) correcte = true;
			else System.out.println("Error, la casella ja està ocupada");
		} while(!correcte);
		
		if (jugadorActual == 1) tauler[fila][col] = 'X';
		else tauler[fila][col] = 'O';
		
		
	}

	private static int ontenirNum() {
		// TODO Auto-generated method stub
		boolean correcto = false;
		int num = 0;
		
		do {
			try {
				System.out.print("Número entre 0 i " + (MAX-1) + ": ");
				num = reader.nextInt();
				if (num >= 0 && num < MAX)
					correcto = true;
				else
					System.out.println("Error, s'esperava un número entre 0 i " + (MAX-1));
			}
			catch (Exception e) {
				System.out.println("Error, s'esperava un número enter");
				reader.nextLine();
			}
			
		} while (!correcto);
		
		return num;
	}

	private static void mostrar(char[][] tauler) {
		// TODO Auto-generated method stub
		int i,j;
		
		for (i=0; i < MAX; i++)
		{
		    for (j=0; j < MAX; j++) {
		        System.out.print (tauler[i][j] + "  ");
		    }
		    System.out.println ("\n");
		}
	}

	private static char obtenerResp() {
		// TODO Auto-generated method stub
		char res;
		boolean correcto = true;
		
		do {
			System.out.print("Quieres seguir jugando? (s/n) : ");
			res = reader.next().charAt(0);
			if (res != 's' && res != 'S' && res != 'n' && res != 'N') {
				System.out.println("Se espera s / n. Vuelve a intentarlo");
				correcto = false;
			}
			
		} while (!correcto);
		
		return res;
	}
	
	private static void inicializar (char[][]tauler, char car) {
		int i,j;

		for (i=0; i < MAX; i++)
		    for (j=0; j < MAX; j++)
		        tauler[i][j] = car;
	}

}


